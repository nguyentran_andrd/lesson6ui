package com.dev.lesson6ui;

import android.app.Application;

public class App extends Application {
    private static App instance;

    private String userName, passWord;
    private boolean rsRegister;

    public static App getInstance() {
        return instance;
    }

    public static void setInstance(App instance) {
        App.instance = instance;
    }

    public void setRsRegister(boolean rsRegister) {
        this.rsRegister = rsRegister;
    }

    public boolean isRsRegister() {
        return rsRegister;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }
}
