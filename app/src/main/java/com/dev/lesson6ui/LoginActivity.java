package com.dev.lesson6ui;

import android.content.Intent;
import android.os.Handler;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class LoginActivity extends BaseActivity {

    private EditText edtUserName, edtpass;
    private ImageView btHome;
    private boolean isExit = false;


    @Override
    protected int getLayoutId() {
        return R.layout.act_login;
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (App.getInstance().isRsRegister()) {
            Toast.makeText(this, "Dang ki thanh cong!", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, "Dang ki that bai!", Toast.LENGTH_LONG).show();
        }
    }

    protected void initView() {
        edtUserName = findViewById(R.id.edt_user_name, this);
        edtpass = findViewById(R.id.edt_password, this);
        btHome = findViewById(R.id.img_home, this);

        findViewById(R.id.bt_login, this);
        findViewById(R.id.tv_register, this);

    }


    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.bt_login) {
            if (textOf(edtUserName).isEmpty() || textOf(edtpass).isEmpty()) {
                Toast.makeText(this, "is empty", Toast.LENGTH_LONG).show();
                return;
            }
            Toast.makeText(this, "Dang nhap thanh cong", Toast.LENGTH_LONG).show();
        } else if (v.getId() == R.id.img_home) {
            delayExitApp();

        } else {
            App.getInstance().setUserName(textOf(edtUserName));
            App.getInstance().setPassWord(textOf(edtpass));
            startActivity(new Intent(this, RegisterActivity.class));
        }
    }

    private void delayExitApp() {
        if (isExit) {
            finish();
            return;
        }

        isExit = true;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(LoginActivity.this, "Back one more time to Exit!", Toast.LENGTH_SHORT).show();
                isExit = false;
            }
        }, 1000);
    }


}
