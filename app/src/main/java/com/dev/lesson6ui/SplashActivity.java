package com.dev.lesson6ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.Nullable;

public class SplashActivity extends BaseActivity {


    @Override
    protected int getLayoutId() {
        return R.layout.act_splash;
    }

    @Override
    protected void initView() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doTaskDelay();
            }
        }, 2000);
    }

    private void doTaskDelay() {
        startActivity(new Intent(this, LoginActivity.class));
//        finish(); cách 1
    }


}
