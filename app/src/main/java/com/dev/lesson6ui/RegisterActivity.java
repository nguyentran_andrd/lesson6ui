package com.dev.lesson6ui;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class RegisterActivity extends BaseActivity {

    private static final String TAG = RegisterActivity.class.getName();
    private EditText edtUserName, edtPass, edtCfPass;
    private Button btRegister;
    private ImageView imgBack;


    @Override
    protected int getLayoutId() {
        return R.layout.act_register;
    }

    private void initData() {
        edtUserName.setText(App.getInstance().getUserName());
        edtPass.setText(App.getInstance().getPassWord());


    }

    protected void initView() {
        edtUserName = findViewById(R.id.edt_user_name_re, this);
        edtPass = findViewById(R.id.edt_password_re, this);
        edtCfPass = findViewById(R.id.edt_cf_pass_re, this);
        btRegister = findViewById(R.id.bt_register, this);
        imgBack = findViewById(R.id.img_back, this);

        initData();

    }


    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.bt_register) {
            if (textOf(edtUserName).isEmpty() || textOf(edtPass).isEmpty() || textOf(edtPass).isEmpty()) {
                Toast.makeText(this, "Hay nhap day du ", Toast.LENGTH_SHORT).show();
                return;
            }

            if (!textOf(edtCfPass).equals(textOf(edtPass))) {
                Toast.makeText(this, "Cf pass not true", Toast.LENGTH_LONG).show();
                return;
            }

            App.getInstance().setRsRegister(true);
            finish();
        } else {
            onBackPressed();
        }


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        App.getInstance().setRsRegister(false);
        finish();
    }
}
